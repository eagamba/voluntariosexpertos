<?php 
require 'code/config.php';
require 'code/funciones.php';


$conexion=conexion($bd_config);
if(!$conexion){
	echo 'no hay conexion';
}
$desastre;
//if($_SERVER['REQUEST_METHOD']== 'GET'){
//$desastre=$_GET['emer'];
//}
$profesiones = listaProfesion($conexion);
//codigo para rellenar tabla\

if($_SERVER['REQUEST_METHOD']== 'POST'){
	$desastre=$_GET['emer'];
	$lugar=$_POST['lugar'];
	$hora=$_POST['hora'];
	$cantidad=$_POST['cantidad'];
	$especialidad=$_POST['especialidad'];
    $cantidadProfesionales = count($especialidad);
	$cantidades = implode("*", $cantidad);
	$especialidades = implode("*", $especialidad);
	registraSolicitudDesastre($conexion,$desastre,$lugar,$hora,$cantidadProfesionales,$especialidades,$cantidades);
	
    // Debes editar las próximas dos líneas de código de acuerdo con tus preferencias
$email_from="formulario@tweencode.com";
$email_to = "ediman3d@gmail.com";
$email_cc = "wiiliiam.delgado@gmail.com";

$email_subject = "Solicitud de voluntariado";

 //En esta parte el valor "name"  sirve para crear las variables que recolectaran la información de cada campo
    $first_name = $_POST['nombre']; // requerido
    $email_from = $_POST['correo']; // requerido
    $email_message = "colaboranos"; // requerido



			//Se crean los encabezados del correo
 $headers = 'From: '.$email_from."\r\n". 'Reply-To: '.$email_to."\r\n". 'Cc: '.$email_cc."\r\n". 'X-Mailer: PHP/'. phpversion(); 
 	
	if (mail($email_to, $email_subject, $email_message, $headers)) {
		header('location:index.php');
	} else {
		echo"Se ha producido un error en el envío del email";
		}
	
    
    
   header('location:asignado.php?c='.$cantidadProfesionales.'&e='.$especialidades.'&q='.$cantidades);
}

?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Voluntarios Expertos</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
<!--		<link href="https://fonts.googleapis.com/css?family=Muli:300,400,700|Raleway:300,400,700" rel="stylesheet">-->
		<link rel="stylesheet" href="css/estilo.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

	</head>
<body>
    <div class="container-fluid">
        <div class="row" style="background-color:dodgerblue;">
          <div class="container">
              <div class="row bg d-flex justify-content-end">
                    <header>
                <nav class="navbar navbar-expand-lg ">
  <a style="color:white!important;text-decoration:none!important" class="navbar-brand" href="index.php"><img src="images/GESTOR.png" style="width:60px;"> Voluntarios Expertos</a>

                </nav>
            </header>
              </div>
              
          </div>
        </div>

   <div class="container">
               <section class="d-flex justify-content-center">
                  
                   <h1>Seleccione la cantidad de voluntarios expertos en <span style="color:darkred"><?php echo $_GET['emer'] ?></span> que necesita</h1>
               </section>
               
               
                <form id="espForm"  action="" name="especialidadesForm" method="post">
							<br>
							<div id="campos">
							<div class="row d-flex justify-content-end">
								    <div class="col-sm-6 col-lg-4">
										<label for="">Lugar</label>
										<input id="lugar1" type="text" class="form-control " name="lugar" placeholder="" required>
										</div>
										<div class="col-sm-6 col-lg-2">
										<label for="">Tiempo reaccion</label>
										<input id="hora1" type="number" class="form-control " name="hora" placeholder="" required>
										</div>
								</div>
								<br>
								<div style="background-color:#f2f2f2;padding:5px 20px 5px 20px;border-radius:10px;border:1px solid #e4e4e4;margin-bottom:10px">
								
								<div class="row" style="margin-bottom:10px;">
								
									<div class="col-sm-6 col-lg-4">
										<label for="">Cantidad</label>
										<input id="cantidad1" type="text" class="form-control " name="cantidad[]" placeholder="" required>
										</div>
										
									<div class="col-sm-6 col-lg-8">
										<label for="">Especialidad</label>
										<select class="form-control" name="especialidad[]" id="esp1">
										<option value="" selected>Seleccione especialidad</option>
										<?php foreach($profesiones as $prof): ?>
										
												<option class="form-control" value="<?php echo $prof['idProf']; ?>">
													<?php echo $prof["profesion"]; ?>
												</option>
								
													<?php endforeach; ?>
									</select>
										</div>
										
								</div>
							</div>
							</div>
							<div class="row" style="margin-bottom:20px;">
								<div class="col-md-4 col-lg-4 text-center">
									<button type='submit' class="btn btn-success btn-block"><i class="fa fa-user"></i> Invitar</button>
								</div>
								<div class="col-md-4 col-lg-4 text-center">
									<button type="button" class="btn btn-primary btn-block" onclick="AgregarCampos()"><i class="fa fa-plus"></i> Agregar voluntario</button>
								</div>
								<div class="col-md-4 col-lg-4 text-center"><a href="index.php" class="btn btn-secondary btn-block"><i class="fa fa-chevron-circle-left"></i> Volver</a></div>
							</div>
						</form>   
               
        </div>
   <footer>
       &copy; Tweencode - Todos los derechos reservados - 2018
   </footer>    
    </div>
    <script type="text/javascript">
	var nextinput = 1;
        var contenido;
        var js = [<?php echo json_encode($profesiones); ?>];
	function AgregarCampos() {
            console.log(js);
        for(var i =0;i<js[0].length;i++){
           contenido+='<option class="form-control" value="'+js[0][i].idProf+'">'+js[0][i].profesion+'</option>'
        }
		nextinput++;
		campo = '<div style="background-color:#f2f2f2;padding:10px 20px 3px 20px;border-radius:10px;border:1px solid #e4e4e4;margin-bottom:10px"><div class="row" style="margin-bottom:10px;"><hr><div class="col-sm-6 col-lg-4"><label for="">Cantidad</label><input id="cantidad' + nextinput + '" type="text" class="form-control " name="cantidad[]" placeholder="" required></div><div class="col-sm-6 col-lg-8"><label for="">Especialidad</label><select class="form-control" name="especialidad[]" id="esp'+nextinput+'"><option value="" selected>Seleccione especialidad</option>'+contenido+'</select></div></div></div>';
		$("#campos").append(campo);
        contenido="";
	}
</script>
</body>
</html>