(function($){
    var cajas = $(".caja"),
        tn = new TimelineLite();
    tn.staggerFromTo(cajas,0.2,{cycle:{
        x:[50,-50],
        scale:[2,50]
    },autoAlpha:0,ease:Power1.easeOut},0.1);
})(jQuery);