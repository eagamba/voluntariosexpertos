<?php

function conexion($bd_config){
  try{
//    $conexion=new PDO('mysql:host=127.0.0.1:3306;dbname='.$bd_config['baseDatos'],$bd_config['usuario'],$bd_config['pass']);
    $conexion=new PDO('mysql:host=163.172.117.143;dbname='.$bd_config['baseDatos'],$bd_config['usuario'],$bd_config['pass']);
    return $conexion;
  }
  catch(PDOException $ex){
    return false;
  }
}

function limpiarDatos($datos){
  $datos = trim($datos);
  $datos = stripslashes($datos);
  $datos = htmlspecialchars($datos);
  $datos = strtolower($datos);
  $datos = filter_var($datos,FILTER_SANITIZE_STRING);
  return $datos;
}

function listaProfesion($conexion){
    $statement=$conexion->query("call sp_listaProfesion()");
    $statement=$statement->fetchAll();
    return ($statement) ? $statement : false;
}
function registraSolicitudDesastre($conexion,$desastre,$lugar,$hora,$cantidadProfesionales,$especialidades,$cantidades){
    $statement=$conexion->query("call sp_registraSolicitudDesastre('$desastre','$lugar','$hora',$cantidadProfesionales,'$especialidades','$cantidades')");
    $statement=$statement->fetchAll();
    return ($statement) ? $statement : false;
}
function listaUsuarios($conexion,$a,$b,$c){
    $statement=$conexion->query("call sp_listaUsuarios($a,'$b','$c')");
    $statement=$statement->fetchAll();
    return ($statement) ? $statement : false;
}
?>