<!DOCTYPE HTML>
<html>
	<head>
		<title>Voluntarios Expertos</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
<!--		<link href="https://fonts.googleapis.com/css?family=Muli:300,400,700|Raleway:300,400,700" rel="stylesheet">-->
    <link rel="icon" href="favicon.ico">
		<link rel="stylesheet" href="css/estilo.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

	</head>
<body>
    <div class="container-fluid">
        <div class="row" style="background-color:dodgerblue;">
          <div class="container">
              <div class="row bg d-flex justify-content-end">
                    <header>
                <nav class="navbar navbar-expand-lg ">
  <a style="color:white!important;text-decoration:none!important" class="navbar-brand" href="index.php"><img src="images/GESTOR.png" style="width:60px;" alt=""> Voluntarios Expertos</a>

                </nav>
            </header>
              </div>
              
          </div>
        </div>

   <div class="container">
               <div class="row">
               <section class="d-flex justify-content-center">
                   <h1>Escoja la emergencia que se esta presentando</h1>
               </section>
               </div>
               <div class="row d-flex justify-content-center">
               
                    <article id="img1" class="caja col-sm-12 col-lg-3 d-flex align-items-center justify-content-center">
                    <a href="emergencia.php?emer=incendios">
                    <img class="icon" src="images/incendio.svg" alt=""><h5 style="margin-left:10px;"> Incendios</h5>
                    </a>
                        
                    </article>
                    <article id="img2" class="caja col-sm-12 col-lg-3 d-flex align-items-center justify-content-center"><a href="emergencia.php?emer=avalancha">
                        
                    <img class="icon" src="images/avalancha.svg" alt=""><h5 style="margin-left:10px;"> Avalancha</h5>
                    </a>
                    </article>
                    <article id="img3" class="caja col-sm-12 col-lg-3 d-flex align-items-center justify-content-center"><a href="emergencia.php?emer=terremoto">
                        
                    <img class="icon" src="images/terremoto.svg" alt=""><h5 style="margin-left:10px;"> Terremoto</h5>
                    </a>
                    </article>
                    <article id="img4" class="caja col-sm-12 col-lg-3 d-flex align-items-center justify-content-center"><a href="emergencia.php?emer=tsunami">
                        
                    <img class="icon" src="images/tsunami.svg" alt=""><h5 style="margin-left:10px;"> Tsunami</h5>
                    </a>
                    </article>
                    <article id="img5" class="caja col-sm-12 col-lg-3 d-flex align-items-center justify-content-center"><a href="emergencia.php?emer=inundacion">
                        
                    <img class="icon" src="images/inundacion.svg" alt=""><h5 style="margin-left:10px;"> Inundacion</h5>
                    </a>
                    </article>
                    <article id="img6" class="caja col-sm-12 col-lg-3 d-flex align-items-center justify-content-center"><a href="emergencia.php?emer=huracan">
                        
                    <img class="icon" src="images/huracan.svg" alt=""><h5 style="margin-left:10px;"> Huracan</h5>
                    </a>
                    </article>
                    <article id="img7" class="caja col-sm-12 col-lg-3 d-flex align-items-center justify-content-center"><a href="emergencia.php?emer=volcan">
                        
                    <img class="icon" src="images/volcan.svg" alt=""><h5 style="margin-left:10px;"> Volcan</h5>
                    </a>
                    </article>
        </div>
        <br>
        <br>
        <br>
        <br>
        <br>
   </div>
   <footer>
       &copy; Tweencode - Todos los derechos reservados - 2018
   </footer>
    </div>
<!--
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    
	<script src="./js/jquery.gsap.min.js"></script>
	<script src="./js/TimelineLite.min.js"></script>
    <script src="./js/animaciones.js"></script>
-->

</body>
</html>