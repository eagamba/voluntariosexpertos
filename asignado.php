<?php 
require 'code/config.php';
require 'code/funciones.php';


$conexion=conexion($bd_config);
if(!$conexion){
	echo 'no hay conexion';
}

if($_SERVER['REQUEST_METHOD']== 'GET'){
	$cantidad=$_GET['q'];
	$especialidad=$_GET['e'];
    $cantidadProfesionales = $_GET['c'];
    $usuarios = listaUsuarios($conexion,$cantidadProfesionales,$especialidad,$cantidad);
}

?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Voluntarios Expertos</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
<!--		<link href="https://fonts.googleapis.com/css?family=Muli:300,400,700|Raleway:300,400,700" rel="stylesheet">-->
		<link rel="stylesheet" href="css/estilo.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

	</head>
<body>
    <div class="container-fluid">
        <div class="row" style="background-color:dodgerblue;">
          <div class="container">
              <div class="row bg d-flex justify-content-end">
                    <header>
                <nav class="navbar navbar-expand-lg ">
  <a style="color:white!important;text-decoration:none!important" class="navbar-brand" href="index.php"><img src="images/GESTOR.png" style="width:60px;" alt=""> Voluntarios Expertos</a>

                </nav>
            </header>
              </div>
              
          </div>
        </div>


   <div class="container">
            
               <section class="d-flex justify-content-center">
                   <h1>Confirmaciones de los expertos</h1>
               </section>
               
               <table style="background-color:white;text-align:center" class="table table-striped">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nombre</th>
      <th scope="col">Telefono</th>
      <th scope="col">Telefono 2</th>
      <th scope="col">Profesion</th>
      <th scope="col">Pendiente</th>
      <th scope="col">En sitio</th>
      <th scope="col">Despachado</th>
    </tr>
  </thead>
  <tbody>
   <?php foreach($usuarios as $usuario): ?>
    <tr>
      <th scope="row"><?php echo $usuario['id'] ?></th>
      <td><?php echo $usuario['nombre'] ?></td>
      <td><?php echo $usuario['tel'] ?></td>
      <td><?php echo $usuario['tel2'] ?></td>
      <td><?php echo $usuario['prof'] ?></td>
      <td><input type="checkbox"></td>
      <td><input type="checkbox"></td>
      <td><input type="checkbox"></td>
   
    </tr>
   <?php endforeach;?>
  </tbody>
</table>
               
                  
               
        </div>
   <footer>
       &copy; Tweencode - Todos los derechos reservados - 2018
   </footer>    
    </div>
</body>
</html>